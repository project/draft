// $id Javascript portion for the Draft Module

/**
 * Creating our namespace for the module so we do not conflict
 * with any other modules that will utilize the same function names
 */
Drupal.draft = {};

if (Drupal.jsEnabled) {
  $(document).ready(function() {
    var str = '<div class="draft_save" id="draft_save">' +
              '<div class="title">Saving Draft</div>' +
              '<div class="body">Draft is currently being saved</div>' +
              '</div>';
    $('body').append(str);
  });
}

/**
 * Function for setting the timeout to save the draft information
 */
Drupal.draft.saveTimeout = function() {
  setTimeout('Drupal.draft.saveForm()', Drupal.settings.draft.interval);
}

/**
 * Function to save the form
 */
Drupal.draft.saveForm = function() {
  $('#' + Drupal.settings.draft.button_id).attr('disabled', 'disabled');
  $('.draft_save').css({top: '600px', left: '600px'}).show();
  // tinymce support taken from the autosave module
  if (typeof tinyMCE == 'object') {
    editorBookmark = tinyMCE.selectedInstance.selection.getBookmark();
    editorIsDirty=true;
    tinyMCE.triggerSave();
  }
  // adding FCKEditor support
  if (typeof(FCKeditor) != 'undefined' && fckLaunchedTextareaId.length) {
    for ( var i = 0; i < fckLaunchedTextareaId.length; i++) {
      var text = FCKeditorAPI.GetInstance( fckLaunchedJsId[i] ).GetXHTML();
      document.getElementById( fckLaunchedTextareaId[i] ).value = text;
    }
  }

  // utilizing the jquery.fields.js plugin here for form hash
  var form_data = $('#' + Drupal.settings.draft.form_id).formHash();
  // removing the tokens we specified in the configuration of the module
  var tokens = Drupal.settings.draft['form_elements_ignore'].split('|');
  for (var x = 0; x < tokens.length; x++) {
    var token = tokens[x];
    delete form_data[token];
  }
  form_data['node_type'] = Drupal.settings.draft.node_type;
  // sending the data back to the server for saving the draft format
  $.ajax({
    url: Drupal.settings.draft.url,
    type: "POST",
    dataType: "xml/html/script/json",
    data: form_data,
    success: function(draft_id, str) {
      $('#edit-draft-id').val(draft_id);
    },
    complete: function() {
      $('#' + Drupal.settings.draft.button_id).removeAttr('disabled');
      setTimeout(function() { $('.draft_save').hide(); }, 2000);
    },
  });

  if (typeof tinyMCE == 'object') {
    tinyMCE.selectedInstance.selection.moveToBookmark(editorBookmark);
  }

  // if autosave is enabled for the node then we continually call the save function
  if (Drupal.settings.draft.autosave) {
    Drupal.draft.saveTimeout();
  }
  return false;
}

/**
 * Function re-populates the form with the information
 */
Drupal.draft.populateForm = function() {
  $('#' + Drupal.settings.draft.form_id).formHash(Drupal.settings.draft.form_data);

  // for FCKEditor
  if (typeof(FCKeditor) != 'undefined' && fckLaunchedTextareaId.length) {
    for ( var i = 0; i < fckLaunchedTextareaId.length; i++) {
      var text = document.getElementById(fckLaunchedTextareaId[i]).value;
      if (text) {
        FCKeditorAPI.GetInstance(fckLaunchedJsId[i]).InsertHtml(text);
      }
    }
  }
}
