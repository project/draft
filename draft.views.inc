<?php

/**
 * @file
 * Provide views data and handlers for draft.module
 */
/**
 * Implementation of hook_views_handlers().
 */
function draft_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'draft') .'/includes',
    ),
    'handlers' => array(
      'draft_views_handler_field_date_drafts_updated' => array(
        'parent' => 'views_handler_field_date',
      ),
      'draft_views_handler_field_draft_data' => array(
        'parent' => 'views_handler_field',
      ),
      'draft_views_handler_field_draft_view' => array(
        'parent' => 'views_handler_field',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data()
 */
function draft_views_data() {
  // field group
  $data['drafts']['table']['group']  = t('Draft');

  // advertise as base table
  $data['drafts']['table']['base'] = array(
    'field' => 'draft_id',
    'title' => t('Draft'),
    'help' => t("Drafts table holding all draft content not yet published"),
    'weight' => -10,
  );

  // table joins
  $data['drafts']['table']['join'] = array(
    'users' => array(
      'left_field' => 'uid',
      'field' => 'uid',
      'type' => 'INNER'
    ),
    'node_type' => array(
      'left_field' => 'node_type',
      'field' => 'type',
      'type' => 'INNER'
    )
  );

  // draft_id
  $data['drafts']['draft_id'] = array(
    'title' => t('Draft ID'),
    'help' => t('The Draft ID'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    )
  );

  // uid
  $data['drafts']['uid'] = array(
    'title' => t('User ID'),
    'help' => t("The user's ID"),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    )
  );

  // node type
  $data['drafts']['node_type'] = array(
    'title' => t('Node Type'),
    'help' => t('Node Type for the draft'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    )
  );

  // data saved about the draft
  $data['drafts']['data'] = array(
    'title' => t('Title'),
    'help' => t('Data title'),
    'field' => array(
      'handler' => 'draft_views_handler_field_draft_data',
      'click sortable' => TRUE,
    )
  );

  // updated
  $data['drafts']['updated'] = array(
    'title' => t('Updated'),
    'help' => t('Last update for draft'),
    'field' => array(
      'handler' => 'draft_views_handler_field_date_drafts_updated',
      'click sortable' => TRUE,
    )
  );

  // this is a link
  $data['drafts']['view_draft'] = array(
    'field' => array(
      'title' => t('View Draft'),
      'help' => t('Provide a simple link to view the draft.'),
      'handler' => 'draft_views_handler_field_draft_view',
    ),
  );
  return $data;
}



