<?php

/**
 * Field handler to display a view link
 */
class draft_views_handler_field_draft_view extends views_handler_field {
  // An example of field level access control
  function access() {
    return user_access('access save as draft');
  }

  function query() {
    $this->ensure_my_table();
    $this->add_additional_fields(array('drafts_node_type' => 'node_type'));
  }

  function render($values) {
    return l(t('View'), 'node/add/'. $values->drafts_node_type, array('query' => array('draft_id' => $values->drafts_draft_id)));
  }
}

?>
